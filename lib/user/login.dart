import 'package:debito/constants/constants.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:hexcolor/hexcolor.dart';

class login extends StatefulWidget {
  @override
  _loginState createState() => _loginState();
}

class _loginState extends State<login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.width,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.width,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              HexColor(Constants.color_primary),
                              HexColor(Constants.color_sec)
                            ],
                            stops: [
                              0.5,
                              0.9
                            ]),
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(70))),
                  ),
                  Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Icon(
                        Icons.widgets_outlined,
                        color: Colors.white,
                        size: 60,
                      )),
                  Positioned(
                      bottom: 30,
                      right: 20,
                      child: Text(
                        "Login",
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: Column(
                children: [
                  SizedBox(height: 20,),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      child: TextField(

                        decoration: InputDecoration(
                          hintText: "Email",
                          prefixIcon: Icon(Icons.markunread,color: HexColor(Constants.color_shade),),

                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                          ),


                        )
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      child: TextField(

                          decoration: InputDecoration(
hintText: "Password",
                            prefixIcon: Icon(Icons.lock_sharp,color: HexColor(Constants.color_shade),),

                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(8),
                            ),


                          )
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [

                        Text("Forgot Password")

                      ],
                    ),
                  )


                ],
              ),
            ),
            Expanded(child: Center(
              child: Container(
                height: 30,
                width: MediaQuery.of(context).size.width-40,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomCenter,
                        colors: [
                          HexColor(Constants.color_primary),
                          HexColor(Constants.color_sec)
                        ],
                        stops: [
                          0.5,
                          0.9
                        ]),
                    borderRadius:
                    BorderRadius.all(Radius.circular(30))),

                child: Center(child: Text("Login",style: TextStyle(color: Colors.white),)),
              ),
            )),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Dont have an accounr?"),
                  Text("Register",style: TextStyle(color: HexColor(Constants.color_primary)),),

                ],
              ),
            ),
            SizedBox(
              height: 20,
            )

          ],
        ),
      ),
    );
  }
}
