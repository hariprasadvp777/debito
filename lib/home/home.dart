import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';

class home extends StatefulWidget {
  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),

      drawer: Drawer(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width-40,
          child: Expanded(child: ListView(
            children: [

              Container(
                child: Text("home"),
              ),
              Divider(
                height: 2,
                thickness: 2,
                color: Colors.red,
              )


            ],

          )),
        ),

      ),

      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            Text("Hello",textAlign: TextAlign.justify,)

          ],
        ),
      ),
    );
  }
}
